# git-demo

## Purpose

The purpose of this source code respository is to demonstrate the distributed version control system known as "Git" and the `git` utility.

## Common Git Commands

- `git commit`
- `git log`
- `git reflog`
- `git push`
